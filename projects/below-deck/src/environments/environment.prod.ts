export interface EnvironmentVar {
    NAME: string,
    URL: string,
    WSPORT: number,
    DEBUG: Debug,
}

export enum Debug {
    DEBUG = 0,
    INFO = 1,
    WARN = 2,
    ERROR = 3,
    FATAL = 4,
}


export const environment: EnvironmentVar = {
    NAME: 'Production',
    URL: 'buccaneer.nmorrissey.com/ws',
    WSPORT: 80,
    DEBUG: Debug.WARN,
}

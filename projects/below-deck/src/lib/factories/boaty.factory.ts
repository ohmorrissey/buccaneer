import { Injectable } from '@angular/core';
import * as THREE from 'three';
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader';
import { Boaty } from '../classes/boaty';
import { LoadingService } from '../services/rendering/loading.service';

const LOADER_ID = 'boaty_models';

const boatyDetail = {
  hull: { name: 'BodyMain', colour: 0xa47449 },
  trim: { name: 'BodyHighlights', colour: 0xdea15f },
  cannons: { name: 'Cannons', colour: 0x222222 },
  sail: { name: 'Sail', colour: 0xeeeeee },
};

/**
 * Parameters for Boaty creation.
 */
export interface BoatyOptions {
  /** Colour of Sail */
  colour?: string;
  /** Boaty username */
  userName?: string;
  /** Should the position be interpolated between frames? */
  isTheClients?: boolean;
}

/**
 * Boaty Factory is responsible for Creating and storing Boaty instances
 * and loading all the OBJ models for the Boaties.
 */
@Injectable({ providedIn: 'root' })
export class BoatyFactory {
  /** Original loaded boaty model. Only loaded once. */
  private boatyModel = {
    hull: new THREE.Group(),
    trim: new THREE.Group(),
    cannons: new THREE.Group(),
    sail: new THREE.Group(),
  };
  /** Has the boaty model been fully loaded? */
  public boatyLoaded = false;

  /** Has each part of boaty been loaded? */
  private boatyPartsLoaded = {
    hull: false,
    trim: false,
    cannons: false,
    sail: false,
  };

  /** Map of all Boaties */
  public readonly boaties = new Map<string, Boaty>();

  constructor(private loadingService: LoadingService) {
    console.log('Boaty Factory Started: Boaty Model Loading');
    this.loadingService.halt(LOADER_ID);
    this.loadBoatyModel();
  }

  /**
   * Loads boaty model from Website Assets
   */
  loadBoatyModel() {
    if (!this.boatyLoaded) {
      this.loadPart(this.boatyModel.hull, boatyDetail.hull);
      this.loadPart(this.boatyModel.trim, boatyDetail.trim);
      this.loadPart(this.boatyModel.cannons, boatyDetail.cannons);
      this.loadPart(this.boatyModel.sail, boatyDetail.sail);
    }
  }

  /**
   * Loads and stores an OBJ model.
   * @param model Object to store model.
   * @param detail Name and colour of Model.
   */
  private loadPart(model: THREE.Object3D, detail: any): void {
    // instantiate a loader
    const loader = new OBJLoader();

    loader.load(
      `../../../assets/Boat_${detail.name as string}.obj`,
      // Callback AFTER resource is loaded
      (object) => {
        // Colouring the boat Model
        this.colourObj(object, detail.colour as number);

        // Moving boat model into position
        object.scale.set(25.4, 25.4, 25.4);
        object.rotateX(-Math.PI / 2);
        object.translateX(-80);
        object.translateZ(-73);
        // console.log(this.boatyModel.hull, model);
        this.assignPart(object, detail.name);
        this.hasEachModelLoaded();

        this.boatyLoaded = true;
      },
      // Callback WHILE object is being loaded
      function (xhr) {
        // console.log(`Loading Boaty ${detail.name}`);
        // console.log(Math.floor(xhr.loaded / xhr.total * 100) + '% loaded');
      },
      // Callback if load FAILS
      function (error) {
        console.log('An error happened', error);
      }
    );
  }

  /** Assigns the incoming object to the correct locally stored model. */
  private assignPart(object: THREE.Group, name: any): void {
    switch (name) {
      case boatyDetail.hull.name: {
        this.boatyModel.hull = object;
        this.boatyPartsLoaded.hull = true;
        break;
      }
      case boatyDetail.trim.name: {
        this.boatyModel.trim = object;
        this.boatyPartsLoaded.trim = true;
        break;
      }
      case boatyDetail.sail.name: {
        this.boatyModel.sail = object;
        this.boatyPartsLoaded.sail = true;
        break;
      }
      case boatyDetail.cannons.name: {
        this.boatyModel.cannons = object;
        this.boatyPartsLoaded.cannons = true;
        break;
      }
    }
  }

  /** Checks if all models have been loaded */
  private hasEachModelLoaded(): void {
    if (
      this.boatyPartsLoaded.hull &&
      this.boatyPartsLoaded.trim &&
      this.boatyPartsLoaded.sail &&
      this.boatyPartsLoaded.cannons
    ) {
      this.boatyLoaded = true;
      this.loadingService.release(LOADER_ID);
      console.log('All models loaded');
    }
  }

  /** Colours the meshes of an object. */
  private colourObj(object: THREE.Object3D, colour: number): void {
    object.children.forEach((mesh) => {
      if (!mesh) return;
      (mesh as THREE.Mesh).material = new THREE.MeshPhongMaterial({
        color: colour,
        side: THREE.DoubleSide,
      });
    });
  }

  /**
   * Create a new Boaty instance.
   * @returns new Boaty.
   */
  public createNewBoaty(userHash: string, options?: BoatyOptions): Boaty {
    const boaty = new Boaty(this.boatyModel, options);

    this.boaties.set(userHash, boaty);
    return boaty;
  }

  /**
   * Interpolate boaties position for when they are not updated by websockets.
   * When a new position comes in, its gets overwritten.
   * @param time between frames in ms.
   */
  public interpolateBoaties(time: number): void {
    for (let [userHash, boaty] of this.boaties) {
      if (boaty.isTheClients) {
        // console.log(userHash, boaty);
        boaty.move(time);
      }
    }
  }
}

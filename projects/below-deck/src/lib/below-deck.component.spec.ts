import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BelowDeckComponent } from './below-deck.component';

describe('BelowDeckComponent', () => {
  let component: BelowDeckComponent;
  let fixture: ComponentFixture<BelowDeckComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BelowDeckComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BelowDeckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

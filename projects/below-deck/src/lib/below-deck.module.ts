import { NgModule } from '@angular/core';
import { BelowDeckComponent } from './below-deck.component';

/** UNUSED Below Deck Library generated file. */

@NgModule({
  declarations: [
    BelowDeckComponent
  ],
  imports: [
  ],
  exports: [
    BelowDeckComponent
  ]
})
export class BelowDeckModule { }

const colourMap = new Map<string, number>([
  ['red', 0xfa3f3f],
  ['green', 0x5dc856],
  ['blue', 0x7182ff],
  ['yellow', 0xd5e239],
  ['orange', 0xf1bd27],
  ['pink', 0xf163bf],
  ['purple', 0xaa34ee],
  ['white', 0xffffff],
]);

export class ColourHelper {
  /** Converts string from Server into Hex Colour Hash */
  static convertStringToHex(colourString: string): number {
    const colour = colourMap.get(colourString);

    return colour !== undefined ? colour : 0xca9b9b;
  }
}

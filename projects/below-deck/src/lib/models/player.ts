import { IMessage } from './messages';
import * as THREE from 'three';
import { Mast, Rudder } from '../classes/boaty';

export interface IPlayerUpdate extends IMessage {
  // status: Status,
  // health: number,
  colour: string;
  position: THREE.Vector3;
  rotation: THREE.Euler;
  rudder: Rudder;
  mast: Mast;
}

export enum Status {
  Respawn = 'respawn',
  Alive = 'alive',
  Dead = 'dead',
}

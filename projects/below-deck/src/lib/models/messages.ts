export interface IMessage {
  userHash: string;
  time: number;
  type: WSTypes;
}

export interface IChatMessage extends IMessage {
  content: string;
  colour: string;
}

export interface IEventMessage extends IMessage {
  event: string;
  colour: string;
}

// export enum ConnectionEvents {
//   connected = 'connected',
//   disconnect = 'disconnected',
// }

export enum WSTypes {
  Chat = 'chat',
  Position = 'playerUpdate',
  Connection = 'connection',
  Event = 'event',
}

export enum Entities {
  Player = 0,
  Cannon = 1
}

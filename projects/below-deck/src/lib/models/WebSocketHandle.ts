import { IMessage } from "./messages";

/** Handles websocket messages */
export interface IWebSocketHandler {
  // These are used to interface between
  // each handler and the WebSocketClientService.
  // When WS client service recieves a message, it will call the handlers update according to
  // type
  /** Handles the update for a message */
  update(message: IMessage): void;
}

import { Injectable } from '@angular/core';
import { IChatMessage, IEventMessage, WSTypes } from '../models/messages';
import { WebsocketServiceClient } from './websocket.service';
import { IWebSocketHandler } from '../models/WebSocketHandle';
import { MyService } from './my.service';
import { PlayerService } from './player.service';

export interface Event {
  userName: string;
  time: string;
  event: string;
  colour: string;
}

/**
 * Service for handling incoming Event messages and displaying them
 * in the bottom left.
 */
@Injectable()
export class EventMessageService implements IWebSocketHandler {
  /**
   * List of stored and displayed events.
   * For display use only!
   */
  public events: Event[] = [];

  constructor(
    private websocketServiceClient: WebsocketServiceClient,
    private myService: MyService,
    private playerService: PlayerService
  ) {
    console.log('Events Spooling');
    // Adds the handler to the WS Client Service
    this.websocketServiceClient.addToHandlers(WSTypes.Event, this);
  }

  /** @inheritdoc */
  update(update: IEventMessage): void {
    const event: Event = {
      userName: update.userHash.split('/')[0],
      time: this.dateToTimeString(update.time),
      event: update.event,
      colour: update.colour,
    };
    this.events.push(event);

    if (update.userHash === this.myService.userHash) {
      this.myService.setColour(update.colour);
    }

    if (update.type === WSTypes.Event && update.event === 'disconnected') {
      this.playerService.removePlayer(update.userHash);
    }

    // If there are more then 10 events are displayed, delete the oldest.
    while (this.events.length > 10) {
      this.events.splice(0, 1);
    }
  }

  /** converts date format to nice string. */
  public dateToTimeString(input: number): string {
    const date = new Date(input);
    const hour = date.getHours().toString();
    const minute = date.getMinutes().toString();

    return `${hour.length < 2 ? 0 + hour : hour}:${
      minute.length < 2 ? 0 + minute : minute
    }`;
  }
}

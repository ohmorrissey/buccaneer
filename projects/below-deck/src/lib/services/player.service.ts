import { Injectable } from '@angular/core';
import { IPlayerUpdate, Status } from '../models/player';
import { IWebSocketHandler } from '../models/WebSocketHandle';
import { Player } from '../classes/player.class';
import { WebsocketServiceClient } from './websocket.service';
import { ThreeService } from './rendering/THREE.service';
import { WSTypes } from '../models/messages';
import { ObservableService } from './core/observable.service';
import { MyService } from './my.service';
import { Vector3 } from 'three';
import { BoatyFactory } from '../factories/boaty.factory';
import { AppState, StateService } from './core/state.service';

/**
 * NOT YET IMPLEMENTED
 * Handles all Players in game. Client Included.
 * Handles position, score and shot updates.
 */
@Injectable({
  providedIn: 'root',
})
export class PlayerService implements IWebSocketHandler {
  public players = new Map<string, Player>();

  private boatyFactory: BoatyFactory;

  constructor(
    private websocketServiceClient: WebsocketServiceClient,
    private threeService: ThreeService,
    private obserableService: ObservableService,
    private myService: MyService,
    private stateService: StateService
  ) {
    console.log('Player Spooling');
    // Adds the handler to the WS Client Service
    this.websocketServiceClient.addToHandlers(WSTypes.Position, this);
    this.startSub();
    this.boatyFactory = this.threeService.boatyFactory;

    this.stateService.state.subscribe((appState) => {
      if (appState === AppState.disconnected) {
        this.removeAllPlayers();
      }
    });
  }

  /** @inheritdoc */
  update(update: IPlayerUpdate): void {
    // get player from active player list
    let player = this.players.get(update.userHash);

    // if player doesn't exist, create one.
    if (!player) {
      player = this.addPlayer(update);
    }

    if (update.colour !== player.colour) {
      console.warn('colour Mismatch', player);
      player.colour = update.colour;
      player.boaty.setSailColour(player.colour);
    }

    // To be used for interpolation on dodgy connections.
    player.lastPosition = player.boaty.position;
    player.lastRotation = player.boaty.rotation;

    // update position and rotation of player boaty.
    player.boaty.setRotationFromEuler(update.rotation);
    player.boaty.position.addVectors(update.position, new Vector3(0, 0, 0));
    player.boaty.rudder = update.rudder;
    player.boaty.mast = update.mast;
    player.boaty.setSailLength();
  }

  /**
   * Adds a new player
   * @param update update for player from Server
   * @returns
   */
  addPlayer(update: IPlayerUpdate): Player {
    console.log('ADDING', update);
    const userName = update.userHash.split('/')[0];
    const boaty = this.boatyFactory.createNewBoaty(update.userHash, {
      colour: update.colour,
      userName: userName,
    });
    this.threeService.addToScene(`boaty-${update.userHash}`, boaty);
    boaty.setRotationFromEuler(update.rotation);

    // Overrides the current position with the new position. HACK.
    boaty.position.addVectors(update.position, new Vector3(0, 0, 0));

    const player: Player = {
      userHash: update.userHash,
      userName: userName,
      colour: update.colour,
      lastPosition: update.position,
      lastRotation: update.rotation,
      boaty: boaty,
    };

    this.players.set(player.userHash, player);
    console.log(this.players);

    return player;
  }

  public removePlayer(userHash: string): void {
    if (!this.players.has(userHash)) {
      return;
    }
    this.threeService.removeFromScene(`boaty-${userHash}`);
    this.players.delete(userHash);
  }

  public removeAllPlayers(): void {
    for (const [userHash, boaty] of this.players) {
      this.removePlayer(userHash);
    }
  }

  /**
   * On new frame, broadcast current position to server.
   */
  startSub() {
    this.obserableService.subscribe(
      this.threeService.nextFrame,
      (time: any) => {
        if (!this.myService.loggedIn) {
          return;
        }

        this.broadcast();
      }
    );
  }

  /**
   * Broadcast current information to Server
   */
  broadcast() {
    // console.log("Share Info");
    const message: IPlayerUpdate = {
      // status: Status.Respawn,
      // health: 0,
      colour: this.myService.colour,
      // connected: true,z
      //@ts-expect-error
      position: this.myService.myBoaty.position,
      //@ts-expect-error
      mast: this.myService.myBoaty.mast,
      //@ts-expect-error
      rudder: this.myService.myBoaty.rudder,
      userHash: this.myService.userHash,
      time: Date.now(),
      type: WSTypes.Position,
      //@ts-expect-error
      rotation: this.myService.myBoaty.rotation,
    };

    this.websocketServiceClient.sendMessage(message);
  }
}

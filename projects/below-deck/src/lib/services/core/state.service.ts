import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export enum AppState {
  loading = 0,
  loaded = 1,
  connected = 2,
  disconnected = 3,
}

export enum GameState {
  Alive = 0,
  Dead = 1,
  Spawning = 2,
}

@Injectable({
  providedIn: 'root',
})
export class StateService {
  public stateSubject = new BehaviorSubject<AppState>(AppState.loading);
  public state = this.stateSubject.asObservable();

  public gameSubject = new BehaviorSubject<GameState>(GameState.Alive);
  public GameState = this.stateSubject.asObservable();
  constructor() {}
}

import { Injectable, OnDestroy } from "@angular/core";
import { Observable, Subscription } from "rxjs";


@Injectable()
export class ObservableService implements OnDestroy {
  private subscriptions = new Array<Subscription>();

  public subscribe(observable: Observable<any>, callback: CallableFunction): void {
    const sub = observable.subscribe((element: any) => {
      callback(element);
    });
    this.subscriptions.push(sub);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => {
      sub.unsubscribe();
    })
  }
}

import { Injectable, OnInit } from '@angular/core';
import { Boaty } from '../classes/boaty';
import { BoatyFactory } from '../factories/boaty.factory';
import { IEventMessage, WSTypes } from '../models/messages';
import { ObservableService } from './core/observable.service';
import { AppState, StateService } from './core/state.service';
import { ThreeService } from './rendering/THREE.service';
import { WebsocketServiceClient } from './websocket.service';

/** Handles information about the client such as;
 *  - userName
 *  - userHash
 *  - userColour
 */
@Injectable()
export class MyService {
  public userName: string = '';
  public userHash: string = '';
  public colour: string = '';
  public myBoaty: Boaty | undefined;
  public loggedIn = false;

  constructor(
    private threeService: ThreeService,
    private observableService: ObservableService,
    private stateService: StateService
  ) {
    console.log('spooling myservice');
    this.checkForUser();
    this.subscribeToThree();

    this.stateService.state.subscribe((appState) => {
      if (appState === AppState.disconnected) {
        // this.logout();
      }
    });
  }

  /**
   * Gets username from localstorage,
   * @returns Username or empty string
   */
  public getUserNameFromStorage(): string {
    return (this.userName = localStorage.getItem('userName') || '');
  }

  /**
   * Logs user into session
   * @param userName Incoming userName
   */
  public login(userName: string): void {
    userName = userName.replace('/', '-');
    this.userName = userName;
    localStorage.setItem('userName', userName);
    this.generateNewUserHash();
    this.loggedIn = true;
    console.log('Signed in: ', this.userName);
  }

  /**
   * Logs user out of session
   */
  public logout(): void {
    console.log('Signed out, ', this.userName);

    this.loggedIn = false;
    this.userName = '';
    this.userHash = '';
    localStorage.removeItem('userName');
    this.myBoaty?.setSailColour('white');
    this.threeService.removeFromScene(this.userHash);
  }

  /**
   * Generates a new userHash from stored userName and date.Now().
   * @returns Generated UserHash
   */
  public generateNewUserHash(): string {
    this.getUserNameFromStorage();
    return (this.userHash = `${this.userName}/${Date.now()}`);
  }

  /**
   * Checks if user has signed in in previous session
   * @returns
   */
  public checkForUser() {
    const username = this.getUserNameFromStorage();

    if (username === '') {
      console.log('No User Info');
      return;
    }

    this.login(username);
  }

  /**
   * Sets this users colour.
   * @param colour New colour.
   */
  setColour(colour: string): void {
    this.colour = colour;
    this.myBoaty?.setSailColour(colour);
  }

  /**
   * Subscribe to new frames from THREE.
   */
  private subscribeToThree() {
    this.observableService.subscribe(
      this.threeService.nextFrame,
      (time: any) => {
        // If myBoaty has'nt yet been created, create a new one.
        if (!this.myBoaty) {
          console.log('Colour', this.colour);
          this.myBoaty = this.threeService.boatyFactory.createNewBoaty(
            this.userHash,
            { colour: this.colour, isTheClients: false }
          );
          this.threeService.addToScene('my_boaty', this.myBoaty);
        }

        // Move Boaty and camera forwards.
        const vector = this.myBoaty.position.clone();
        this.myBoaty?.move(time);
        this.threeService.camera.position.add(
          this.myBoaty.position.clone().sub(vector)
        );
        this.threeService.orbitControls.target = this.myBoaty.position.clone();
      }
    );
  }
}

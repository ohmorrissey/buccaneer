import { Injectable } from "@angular/core";
import { IChatMessage, WSTypes } from "../models/messages";
import { WebsocketServiceClient } from "./websocket.service";
import { IWebSocketHandler } from "../models/WebSocketHandle";
import { MyService } from "./my.service";

export interface Chat {
  userName: string,
  time: string,
  message: string,
  colour: string,
}

/**
 * Service for handling chat messages and displaying them in the right hand corner.
 */
@Injectable()
export class ChatService implements IWebSocketHandler {

  /** List of Chats stored to display */
  public chats: Chat[] = [];

  constructor(
    private websocketServiceClient: WebsocketServiceClient,
    private myService: MyService,
  ) {
    console.log("Chat Spooling");
    // Adds the handler to the WS Client Service
    this.websocketServiceClient.addToHandlers(WSTypes.Chat, this);
  }

  /** @inheritdoc */
  update(update: IChatMessage): void {

    const chat: Chat = {
      userName: update.userHash.split("/")[0],
      time: ChatService.dateToTimeString(update.time),
      message: update.content,
      colour: update.colour, // this.playerService.getColourbyPlayerHash(update.userHash),
    }
    this.chats.push(chat);

    // If there are more then 10 chats, delete the oldest.
    while (this.chats.length > 10) {
      this.chats.splice(0, 1);
    }
    console.log(update, this.chats)
  }

  /**
   * Sends a chat from the client to other player in the game.
   * @param message String to send.
   * @returns
   */
  sendChat(message: string): IChatMessage {
    const chat: IChatMessage = {
      userHash: this.myService.userHash,
      time: Date.now(),
      type: WSTypes.Chat,
      content: message,
      colour: this.myService.colour,
    }
    this.update(chat);
    this.websocketServiceClient.sendMessage(chat);
    return chat;
  }

  /** converts date format to nice string. */
  public static dateToTimeString(input: number): string {
    const date = new Date(input);
    const hour = date.getHours().toString();
    const minute = date.getMinutes().toString();

    return `${hour.length < 2 ? 0 + hour : hour}:${minute.length < 2 ? 0 + minute : minute}`;
  }

}

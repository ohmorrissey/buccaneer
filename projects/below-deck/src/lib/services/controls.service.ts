import { Injectable } from '@angular/core';
import { Mast, Rudder } from '../classes/boaty';
import { MyService } from './my.service';

/**
 * Service that recieves keyboard inputs and activates the relevant controls
 */
@Injectable({
  providedIn: 'root',
})
export class ControlService {
  /** Map of keys held down */
  heldKeys = new Map<string, boolean>();

  constructor(private myService: MyService) {
    console.log('ControlService');
    document.addEventListener('keydown', (event) => {
      console.log('KEYPRESS;', event);

      if (!this.myService.myBoaty) {
        return;
      }

      this.heldKeys.set(event.key.toLocaleLowerCase(), true);

      switch (event.key) {
        case 'w':
          this.myService.myBoaty.speedUp();
          break;
        case 's':
          this.myService.myBoaty.slowDown();
          break;
        default:
          // do nothing
          break;
      }

      this.handingTurning();
    });

    document.addEventListener('keyup', (event) => {
      if (!this.myService.myBoaty) {
        return;
      }

      this.heldKeys.delete(event.key.toLocaleLowerCase());

      this.handingTurning();
    });
    // this.keyboardService.addListener('Control', this.slowDownCallback);
  }

  /**
   *  Process a turning input
   */
  handingTurning(): void {
    const boaty = this.myService.myBoaty;

    if (!boaty) {
      return;
    }
    if (this.heldKeys.has('a') === this.heldKeys.has('d')) {
      boaty.steer(Rudder.straight);
      return;
    }

    if (this.heldKeys.has('a')) {
      boaty.steer(Rudder.left);
      return;
    }

    if (this.heldKeys.has('d')) {
      boaty.steer(Rudder.right);
      return;
    }
  }
}

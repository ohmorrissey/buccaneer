import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AppState, StateService } from '../core/state.service';
import { ThreeService } from './THREE.service';

@Injectable()
export class LoadingService {
  private halts = new Set<string>();

  constructor(private router: Router, private stateService: StateService) {}

  public halt(id: string) {
    this.halts.add(id);
  }

  public release(id: string) {
    this.halts.delete(id);

    if (!(this.halts.size > 0)) {
      this.load();
    }
  }

  private load(): void {
    this.router.navigate(['/menu']);
    this.stateService.stateSubject.next(AppState.loaded);
  }
}

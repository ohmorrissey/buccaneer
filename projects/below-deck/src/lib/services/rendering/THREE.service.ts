import { Injectable } from '@angular/core';
import { Boaty } from '../../classes/boaty';
import { BoatyFactory } from '../../factories/boaty.factory';
import * as THREE from 'three';
import { Group } from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader';
import { AnimateObject } from '../../classes/animate-object';
import {
  CSS2DRenderer,
  CSS2DObject,
} from 'three/examples/jsm/renderers/CSS2DRenderer.js';
import { ObservableService } from '../core/observable.service';
import { Subject } from 'rxjs';
import { AppState, StateService } from '../core/state.service';
import { DeviceDetectorService } from 'ngx-device-detector';

/** Angle for Camera View */
const VIEW_ANGLE = (2 / 5) * Math.PI;

const DEPLOY_DUMMY = false;

/**
 * Service that handles the overall THREE rendering.
 */
@Injectable()
export class ThreeService {
  /** Is Three Service Initiated? */
  public ThreeRunning: boolean = false;
  private objects = new Map<string, THREE.Object3D<THREE.Event>>();
  public scene = new THREE.Scene();
  public camera = new THREE.PerspectiveCamera(
    45,
    window.innerWidth / window.innerHeight,
    0.1,
    100000
  );
  public renderer = new THREE.WebGLRenderer();
  public orbitControls = new OrbitControls(
    this.camera,
    this.renderer.domElement
  );
  private lastTime: number = Date.now();

  /** Time between frames. */
  private timeDiff = new Subject<number>();
  /** Triggered on new frame and passes the time between last frame. */
  public nextFrame = this.timeDiff.asObservable();

  constructor(
    private stateService: StateService,
    public boatyFactory: BoatyFactory,
    private deviceDetector: DeviceDetectorService
  ) {
    console.log('Three Service Started');
    this.stateService.state.subscribe((value) => {
      if (value !== AppState.loaded) {
        return;
      }

      this.init();
    });
  }

  /**
   * Initiate THREE Service. Loads THREE into webpage body. Loads water and lights.
   * Runs THREE animation.
   * @returns
   */
  init(): void {
    if (this.ThreeRunning) {
      // console.warn('ThreeService already running');
      return;
    }
    console.log('ThreeService Spooling');

    this.ThreeRunning = true;

    this.initialiseWindow();
    this.initialiseScene();
    this.startAnimation();
  }

  /**
   * Starts core THREE aspects, camera, renderer etc.
   */
  initialiseWindow(): void {
    /** Set initial camera and window settings */
    this.camera.position.set(800, 0, 0);
    this.camera.rotation.set(0, 0, 0);
    this.camera.layers.enableAll();
    this.renderer.setSize(window.innerWidth, window.innerHeight);

    //Add the renderer canvas to the DOM.
    document.body.appendChild(this.renderer.domElement);

    /** Locks camera angle  */
    this.orbitControls.minPolarAngle = VIEW_ANGLE;
    this.orbitControls.maxPolarAngle = VIEW_ANGLE;

    /**
     * Resizes THREE scene to the full window.
     */
    const WindowResize = () => {
      this.camera.aspect = window.innerWidth / window.innerHeight;
      this.camera.updateProjectionMatrix();
      this.renderer.setSize(window.innerWidth, window.innerHeight);
    };

    // Listen for a window resize, then resize THREE renderer accordingly
    window.addEventListener('resize', WindowResize, false);
    window.addEventListener('orientationchange', WindowResize);
  }

  /**
   * Starts the THREE Scene related objects, lights, water etc.
   */
  private initialiseScene(): void {
    // Add objects that were added before THREE was initialised.
    for (let [name, obj] of this.objects) {
      console.warn('Adding from Stored: ', name);
      this.scene.add(obj);
    }

    // /** Create Sunlight */
    const sunLight = new THREE.PointLight(0xfdfcf0, 1);
    sunLight.position.set(0, 20000, 20000);
    this.addToScene('sun_light', sunLight);
    const ambientLight = new THREE.AmbientLight(0xfdfcf0, 0.3);
    ambientLight.layers.enableAll();
    this.addToScene('ambient_light', ambientLight);

    /** Adds the sky */
    this.scene.background = new THREE.Color(0xadd8e6);

    /** Adds the Water */
    const waterGeometry = new THREE.CircleGeometry(10000, 10000);
    const waterMaterial = new THREE.MeshPhongMaterial({
      color: 0x3987c9,
      side: THREE.DoubleSide,
    });
    const waterObject = new THREE.Mesh(waterGeometry, waterMaterial);
    waterObject.rotateX(Math.PI / 2);
    this.addToScene('water', waterObject);

    /** Add starter Marker */
    const markerGeometry = new THREE.CircleGeometry(50, 100);
    const markerMaterial = new THREE.MeshPhongMaterial({
      color: 0x3987a0,
      side: THREE.DoubleSide,
    });
    const markerObject = new THREE.Mesh(markerGeometry, markerMaterial);
    markerObject.rotateX((3 * Math.PI) / 2);
    // raise it by 1 so its not in the water
    markerObject.translateZ(1);
    this.addToScene('start_marker', markerObject);

    // Optional dummy boat to deploy.
    if (DEPLOY_DUMMY) {
      const dumbyboaty = this.boatyFactory.createNewBoaty('dummy123', {
        colour: 'white',
        userName: ' dummy',
      });
      dumbyboaty.rotateY(Math.PI / 4);
      this.addToScene('default_boaty', dumbyboaty);
    }
  }

  /**
   * Add an Object to THREE Scene.
   * @param name local name of object.
   * @param object object instance
   */
  public addToScene(name: string, object: THREE.Object3D<THREE.Event>): void {
    // If three isn't running, return
    if (!this.ThreeRunning) {
      console.error('Loaded before THREE spooled ', name);
      return;
    }

    object.name = name;
    this.objects.set(name, object);
    this.scene.add(object);
  }

  public removeFromScene(name: string): void {
    const object = this.objects.get(name);
    if (!object) {
      return;
    }

    this.scene.remove(object);

    this.objects.delete(name);
  }

  /** THREE Animation Loop */
  private startAnimation(): void {
    // Part of initialising orbital controls.
    this.orbitControls.update();
    this.lastTime = Date.now();

    /**
     * Main loop for THREE animation
     */
    const animateTHREE = () => {
      const timeDiff = Date.now() - this.lastTime;
      this.timeDiff.next(timeDiff);
      this.lastTime = Date.now();

      this.boatyFactory.interpolateBoaties(timeDiff);

      requestAnimationFrame(animateTHREE);
      this.renderer.render(this.scene, this.camera);
    };

    // Render First frame
    this.renderer.render(this.scene, this.camera);
    animateTHREE();
  }
}

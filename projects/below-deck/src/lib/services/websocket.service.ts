import { Injectable, OnDestroy } from '@angular/core';
import {
  IChatMessage,
  IEventMessage,
  IMessage,
  WSTypes,
} from '../models/messages';
import { IPlayerUpdate } from '../models/player';
import { IWebSocketHandler } from '../models/WebSocketHandle';
import { environment } from 'projects/below-deck/src/environments/environment';
import { MyService } from './my.service';
import { AppState, StateService } from './core/state.service';

const DEFAULT_WS_URL = `ws://${environment.URL}`;
const IndicatorID = 'WSIndicator';
const urlKey = 'urlKey';

/**
 * Handles the Websocket connection to the server and
 * directs incoming messages to the correct handler.
 */
@Injectable()
export class WebsocketServiceClient implements OnDestroy {
  public url = DEFAULT_WS_URL;

  private webSocket: WebSocket | undefined = undefined;
  /** List of websocket message handlers. */
  public handlers = new Map<WSTypes, IWebSocketHandler>();

  constructor(
    private myService: MyService,
    private stateService: StateService
  ) {
    this.socketIndicator(false);
    console.log('WSService Spooling');

    // Attempt to get WS URL from local storage
    const url = localStorage.getItem(urlKey);

    // If no url in local storage, save and use default
    if (url !== null && url !== '') {
      this.url = url;
    } else {
      localStorage.setItem(urlKey, DEFAULT_WS_URL);
    }

    this.stateService.state.subscribe((appState) => {
      if (appState === AppState.disconnected) {
        this.closeWS();
        this.myService.logout();
      }
    });
  }

  /**
   * Initiate Websocket Connection.
   */
  public init(): void {
    if (this.webSocket?.readyState != WebSocket.OPEN) {
      console.log('Connecting to websocket on:', this.url);
      this.webSocket = new WebSocket(this.url);
      this.webSocket.onopen = this.webSocketOnOpen.bind(this);
      this.webSocket.onmessage = this.webSocketOnMessage.bind(this);
      this.webSocket.onclose = this.webSocketOnClose.bind(this);
    } else {
      console.log('Connection already Established');
    }
  }

  public closeWS(): void {
    if (!this.webSocket) {
      return;
    }

    const disconnectedMessage: IEventMessage = {
      userHash: this.myService.userHash,
      time: Date.now(),
      type: WSTypes.Event,
      event: `disconnected`,
      colour: this.myService.colour,
    };

    this.sendMessage(disconnectedMessage);

    this.webSocket.close();
  }

  /**
   * Callback when websocket is opened.
   */
  private webSocketOnOpen(): void {
    console.log('Connected to websocket on:', this.url);
    const connectedMessage: IEventMessage = {
      userHash: this.myService.userHash,
      time: Date.now(),
      type: WSTypes.Event,
      event: `connected`,
      colour: '',
    };
    this.sendMessage(connectedMessage);
    this.socketIndicator(true);
  }

  /**
   * Add a WS Message handler to the list of handlers.
   * @param type type of message to direct to handler
   * @param item handler.
   */
  public addToHandlers(type: WSTypes, item: IWebSocketHandler): void {
    this.handlers.set(type, item);
    console.log(type, item);
  }

  /**
   * Callback when websocket recieves a message.
   */
  private webSocketOnMessage(message: any): void {
    const dataIn: IMessage = JSON.parse(message.data.toString());
    this.handlers.get(dataIn.type)?.update(dataIn);
  }

  /**
   * Sends an outbound message to the server.
   * @param message
   */
  sendMessage(
    message: IMessage | IEventMessage | IChatMessage | IPlayerUpdate
  ): void {
    if (this.webSocket?.readyState != WebSocket.OPEN) {
      return;
    }
    const dataOut: string = JSON.stringify(message);
    // console.log(dataOut);
    this.webSocket?.send(dataOut);
  }

  /** Callback for websocket closing. */
  private webSocketOnClose(): void {
    this.socketIndicator(false);
  }

  /**
   * Adds or removes the WS Disconnected indicator
   * @param status WS Status.
   */
  private socketIndicator(status: boolean): void {
    if (!status) {
      const doc = document.getElementById('topDiv');
      const sign = document.createElement('div');
      sign.id = IndicatorID;
      sign.setAttribute(
        'style',
        `
          text-align: right;
          position: absolute;
          right: 10px;
          bottom: 10px;
          z-index: 100;
          `
      );
      sign.innerHTML = '🔴';
      if (document.getElementById(IndicatorID) === null) {
        doc?.appendChild(sign);
        console.log('WS Closed, adding indicator');
      }
    } else {
      document.getElementById(IndicatorID)?.remove();
      console.log('WS Open, remove symbol');
    }
  }

  ngOnDestroy(): void {
    this.webSocket?.close();
  }

  /***
   * Pass the service a new URL for WS Connection.
   * Method verifies the connection.
   * @param url, optional, url to check.
   * If not passed, default will be added.
   */
  public setUrl(url?: string) {
    // If sent an empty URL, set default
    if (url === undefined) {
      url = DEFAULT_WS_URL;
    }

    // Attempt to verify new WS url.
    let trialWebsocket: WebSocket;

    const websocketPromise = new Promise((resolve, reject) => {
      trialWebsocket = new WebSocket(url as string);
    });
    websocketPromise.then(
      () => {
        // On successful connection, close connection
        trialWebsocket?.close();
        localStorage.setItem(urlKey, url as string);
        this.url = url as string;
      },
      () => {
        // On failed connection
        console.warn('url: ', url, 'is invalid');
        return;
      }
    );
  }
}

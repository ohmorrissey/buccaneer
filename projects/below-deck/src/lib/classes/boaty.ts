import * as THREE from 'three';
import { BoatyOptions } from '../factories/boaty.factory';
import { ColourHelper } from '../helpers/colour.helper';

/** Formatting for Overhead Name Tag Canvas */
const format = {
  overallSize: 0.04,
  fontSize: 400,
  font: 'Monaco',
};

export enum Rudder {
  left = 1,
  straight = 0,
  right = -1,
}

export enum Mast {
  stop = 0,
  low = 1,
  half = 2,
  full = 3,
}

/**
 * Boat Object as THREE Group.
 * Made of Boat Model, flag and sails.
 * DO NOT CREATE. Use BoatyFactory to create new boats.
 */
export class Boaty extends THREE.Group {
  /** Hull Instance */
  public hull: THREE.Group;
  /** Trim Instance */
  public trim: THREE.Group;
  /** Cannons Instance */
  public cannons: THREE.Group;
  /** Sail Instance */
  public sail: THREE.Group;
  /** Direction the boat is turning */
  public rudder: Rudder = Rudder.left;
  /** Speed of the boat */
  public mast: Mast = Mast.full;
  /** Colour of boaty */
  public colour: string = 'white';
  /**
   * Should boaty position be interpolated.
   * False for MyService.boaty.
   * True for all boaty's incoming over websocket
   */
  public isTheClients = true;
  public sprite = new THREE.Sprite();

  /**
   *
   * @param boatyModel
   * @param colour
   */
  constructor(boatyModel: any, options?: BoatyOptions) {
    // empty super call for THREE.Group
    super();

    // Adds the boat model to the class
    this.hull = boatyModel.hull.clone();
    this.trim = boatyModel.trim.clone();
    this.cannons = boatyModel.cannons.clone();
    this.sail = boatyModel.sail.clone();

    // Adds each sub model to the group
    this.add(this.hull);
    this.add(this.trim);
    this.add(this.cannons);
    this.add(this.sail);

    // Sets parameters
    if (options?.colour !== undefined) {
      this.setSailColour(options.colour);
    }

    if (options?.userName !== undefined) {
      this.createLabel(options.userName);
    }

    if (options?.isTheClients !== undefined) {
      this.isTheClients = options.isTheClients;
    }

    // Adds flag model
    // TODO add flag model

    // Moves boatGroup to water level
    this.translateY(100);
    console.log('New Boaty Instance', this);
  }

  /**
   * Moves boat forwards per unit time.
   * @param time in ms.
   */
  public move(time: number): void {
    for (let i = 0; i < time; i++) {
      // creates unit vector in positive X. Then Rotates it to match
      // the heading of the Boaty. Moves boaty that time.
      const vector = new THREE.Vector3(this.mast / 40, 0, 0);
      vector.applyEuler(this.rotation);
      this.position.sub(vector);

      // Rotates boaty. Only used for demo. To be OBE once controls added.
      this.rotateY(((4 - this.mast) * this.rudder) / 10_000);
    }
  }

  /**
   * Takes a direction of Enum Rudder and applies it to the boaty.
   * @param direction
   */
  public steer(direction: Rudder): void {
    this.rudder = direction;
  }

  /**
   * Lowers the sail and increases speed
   */
  public speedUp(): void {
    this.mast++;

    if (this.mast > Mast.full) {
      this.mast = Mast.full;
    }

    this.setSailLength();
  }

  /**
   * Raises the sail and slows speed
   */
  public slowDown(): void {
    this.mast--;

    if (this.mast < Mast.stop) {
      this.mast = Mast.stop;
    }

    this.setSailLength();
  }

  /**
   * Processes sail length based on Mast Length
   */
  public setSailLength(): void {
    this.sail.scale.z = (this.mast * 21.4 + 12) / 3;
    this.sail.position.y = (3 - this.mast) * 25.4 - 73;
  }

  /**
   * sets the colour of the sail.
   * Converts string from web server to a hex colour key.
   * @param colour
   */
  public setSailColour(colour: string) {
    this.colour = colour;
    this.sail.children.forEach((mesh) => {
      console.log('Attempting Colour', colour);
      if (!mesh) return;
      console.log('colouring Boaty: ', colour);
      if (colour.length < 1) {
        console.error('No colour passed', colour);
        return;
      }
      (mesh as THREE.Mesh).material = new THREE.MeshPhongMaterial({
        color: ColourHelper.convertStringToHex(colour),
        side: THREE.DoubleSide,
      });
    });
  }

  /**
   * Creates a THREE.Sprite out of a html Canvas for the username tag above the boaty.
   * @param labelText
   */
  public createLabel(labelText: string) {
    const canvas = document.createElement('canvas');
    canvas.height = format.fontSize * 1.1;
    canvas.width =
      format.fontSize / 2 + format.fontSize * 0.55 * labelText.length;
    canvas.style.textAlign = 'center';
    canvas.style.display = 'inline';
    canvas.style.width = '100%';

    const context = canvas.getContext('2d') as CanvasRenderingContext2D;
    context.fillStyle = 'rgba(0, 0, 0, 0.5)';
    context.fillRect(0, 0, canvas.width, canvas.height);

    context.font = `${format.fontSize}px ${format.font}`;
    context.textAlign = 'center';
    context.textBaseline = 'middle';
    context.fillStyle = 'rgba(255, 255, 255, 1)';
    context.fillText(labelText, canvas.width / 2, canvas.height / 2);

    const texture = new THREE.Texture(canvas);
    texture.needsUpdate = true;

    const spriteMaterial = new THREE.SpriteMaterial({ map: texture });
    this.sprite = new THREE.Sprite(spriteMaterial);
    this.sprite.position.set(0, 100, 0);
    this.sprite.scale.set(
      format.overallSize * canvas.width,
      1.25 * format.overallSize * canvas.height,
      0
    );
    this.add(this.sprite);
  }
}

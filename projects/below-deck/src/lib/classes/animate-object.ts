import { ConstantPool } from "@angular/compiler";
import { Observable, Subject } from "rxjs";
import { ObservableService } from "../services/core/observable.service";
import { ThreeService } from "../services/rendering/THREE.service";



export abstract class AnimateObject {



  constructor(
    private threeService: ThreeService,
    private observableService: ObservableService,
  ) {
    this.observableService.subscribe(this.threeService.nextFrame, (time: any) => {
      this.nextFrame();
    });

  }

  abstract nextFrame(): void;
}

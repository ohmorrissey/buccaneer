import { IPlayerUpdate, Status } from "../models/player";
import * as THREE from "three";
import { Vector3 } from "three";
import { Boaty } from "./boaty";


export interface Player {
  userHash: string,
  userName: string,
  colour: string,
  boaty: Boaty,
  lastPosition: THREE.Vector3,
  lastRotation: THREE.Euler,
}


/** NOT YET IMPLEMENTED */
// export class Player {
//   public userHash: string = '';
//   public userName: string = '';
//   public latestUpdate: number = 0;
//   public previousUpdate: number = 0;
//   public status: Status = Status.Respawn;
//   public health: number = 0;
//   public score: number = 0;
//   public colour: string = '';
//   public connected: boolean = false;
//   public position: THREE.Vector3 = new Vector3(0, 0, 0);
//   public heading: number = 0;
//   public previousPosition: THREE.Vector3 = new Vector3(0, 0, 0);

//   updateProperties(player: IPlayerUpdate) {
//     this.previousPosition = this.position;
//     this.previousUpdate = this.latestUpdate;

//     this.userName = player.userHash;
//     this.userHash = player.userHash;
//     this.latestUpdate = player.time;
//     // this.status = player.status;
//     // this.health = player.health;
//     this.colour = player.colour;
//     // this.connected = player.connected;
//     this.position = player.position;
//     this.heading = player.heading;
//   }

// }

import { TestBed } from '@angular/core/testing';

import { BelowDeckService } from './below-deck.service';

describe('BelowDeckService', () => {
  let service: BelowDeckService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BelowDeckService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

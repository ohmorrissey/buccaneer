/**
 *
 * A base gulp file taken from a template https://www.typescriptlang.org/docs/handbook/gulp.html
 *
 * instructions for gulp.
 */

var gulp = require("gulp");
var ts = require("gulp-typescript");
var tsProject = ts.createProject("tsconfig.json");
gulp.task("default", function () {
  return tsProject.src().pipe(tsProject()).js.pipe(gulp.dest("dist"));
});
//TODO outputs to dist/projects/crows-nest/src
  // Idealy it'll be dist/src

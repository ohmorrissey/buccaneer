export enum Debug {
  DEBUG = 0,
  INFO = 1,
  WARN = 2,
  ERROR = 3,
  FATAL = 4,
}

export const environment = {
  WSPORT: 3080,
  DEBUG: Debug.WARN,
}

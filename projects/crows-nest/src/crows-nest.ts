import WebSocket, { WebSocketServer } from 'ws';
import { IEventMessage } from '../../below-deck/src/lib/models/messages'
import { IMessage } from '../../below-deck/src/lib/models/messages'
import { environment } from '../environments'

const PORT = environment.WSPORT;

/**
 * List of colours for players.
 */
const COLOURS = [
  'red',
  'green',
  'blue',
  'yellow',
  'orange',
  'pink',
  'purple',
]

/**
 * Back End Server for Buccaneer.
 *
 */
export class ReturnFireServer {
  public webSocketServer = new WebSocketServer({ port: PORT });
  public clients = new Map<string, WebSocket.WebSocket>();
  public colours = new Map<string, any>();

  /**
   * Starts Websocket Server.
   */
  init(): void {
    console.log("ReturnFireServer Listening on port:", PORT);
    this.onWSConnection();
    COLOURS.forEach((colour) => {
      this.colours.set(colour, null);
    })
  }

  /** Callback for a new  websocket connection */
  onWSConnection(): void {
    this.webSocketServer.on('connection', webSocket => {
      console.log("New connection");
      this.onWSMessage(webSocket);
      this.onWSClose(webSocket);
    });
  }

  /** Callback for new message */
  onWSMessage(webSocket: WebSocket.WebSocket): void {
    webSocket.on('message', (dataIn, isBinary) => {
      const message: IMessage = JSON.parse(dataIn.toString());
      console.log('recieved', message);
      this.checkIfNewClient(webSocket, message as IEventMessage);
      this.broadcastAllExcept(message, message.userHash);
    });
  }

  /** Callback for a websocket closing. */
  onWSClose(webSocket: any): void {
    webSocket.on('close', () => {

      console.log(
        "XXX ",
        webSocket.id,
        " disconnected.",
        " XXX"
      );
      const disconnectedMessage: any = {
        userHash: webSocket.id,
        time: Date.now(),
        type: "event",
        event: `disconnected`,
        colour: webSocket.colour,
      }
      console.log(disconnectedMessage);
      this.colours.set(webSocket.colour, null);
      this.clients.delete(webSocket.id);
      this.broadcastAll(disconnectedMessage);
    })
  }

  /**
   * Checks if the client is new. If not, adds them to clients.
   * @param webSocket
   * @param message
   */
  checkIfNewClient(webSocket: any, message: IEventMessage): void {
    if (!this.clients.has(message.userHash)) {
      this.clients.set(message.userHash, webSocket);
      // saves userHash internally. Needed for disconnection broadcasts.
      webSocket.id = message.userHash;

      // Finds the next free colour and assigns it to websocket.
      for (let [colour, ws] of this.colours) {
        if (ws === null) {
          webSocket.colour = colour;
          this.colours.set(colour, webSocket);
          break;
        }
      }
      message.colour = webSocket.colour;
      console.log("Sent: ", message);
      this.broadcast(message.userHash, message as IMessage);
    }
  }

  /**
   * Broadcasts message to each client connected.
   * @param message
   */
  broadcastAll(message: IMessage) {
    for (const [clientName, client] of this.clients) {
      this.broadcast(clientName, message);
    }
  }

  /**
   * Broadcasts a message to each client except one.
   * @param message
   * @param clientToIgnore
   */
  broadcastAllExcept(message: IMessage, clientToIgnore: string) {
    for (const [clientName, client] of this.clients) {
      if (clientName != clientToIgnore) {
        this.broadcast(clientName, message);
      }
    }
  }

  /**
   * Broadcasts a message to a single client.
   * @param clientName
   * @param message
   */
  broadcast(clientName: string, message: IMessage) {
    if (this.clients.get(clientName)?.readyState === WebSocket.OPEN) {
      this.clients.get(clientName)?.send(JSON.stringify(message), { binary: false });
    }
  }
}

// Create and start new server.
const server = new ReturnFireServer();
server.init();


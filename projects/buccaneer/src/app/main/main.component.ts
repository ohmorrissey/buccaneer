import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MyService } from 'projects/below-deck/src/lib/services/my.service';
import { ThreeService } from 'projects/below-deck/src/lib/services/rendering/THREE.service';
import { WebsocketServiceClient } from 'projects/below-deck/src/libs';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {
  constructor(
    private websocketServiceClient: WebsocketServiceClient,
    public myService: MyService,
    private threeService: ThreeService,
    private router: Router
  ) {}

  ngOnInit(): void {
    // this.threeService.init();
  }

  nagivateToLogin() {
    this.router.navigate(['/menu/login']);
  }

  /** Callback for Load button on main screen. */
  joinGame() {
    if (this.myService.loggedIn) {
      // Exit menu
      this.router.navigate(['/']);
      this.websocketServiceClient.init();
    } else {
      // Not really a dev warning, should be replaced by an on screen error.
      console.warn('Cannot Login, Login Error');
    }
  }
}

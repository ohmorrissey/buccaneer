import { Component, OnInit } from '@angular/core';
import { ThreeService } from 'projects/below-deck/src/lib/services/rendering/THREE.service';
import { WebsocketServiceClient } from 'projects/below-deck/src/libs';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  url: string = "";
  urlWSInput!: HTMLInputElement;

  constructor(
    private websocketService: WebsocketServiceClient,
    private threeService: ThreeService,
  ) { }

  ngOnInit(): void {
    // Populate URL from storage
    this.urlWSInput = document.getElementById("setUrl") as HTMLInputElement;
    this.resetPlaceholder();
  }

  /**
   * Sends new url to Websocket Service
   */
  setWSUrl(): void {
    console.log(this.url)
    this.websocketService.setUrl(this.urlWSInput.value);
    this.resetPlaceholder();
  }

  /**
   * Sets the websocket service back to default.
   */
  resetWSUrl(): void {
    this.websocketService.setUrl();
    this.resetPlaceholder();
  }

  /**
   * Sets input value to current url.
   */
  resetPlaceholder(): void {
    this.urlWSInput.value = this.websocketService.url;
  }
}

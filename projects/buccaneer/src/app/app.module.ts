import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ObservableService } from 'projects/below-deck/src/lib/services/core/observable.service';
import { EventMessageService } from 'projects/below-deck/src/lib/services/event.message.service';
import { MyService } from 'projects/below-deck/src/lib/services/my.service';
import { PlayerService } from 'projects/below-deck/src/lib/services/player.service';
import { ThreeService } from 'projects/below-deck/src/lib/services/rendering/THREE.service';
import {
  ChatService,
  WebsocketServiceClient,
} from 'projects/below-deck/src/libs';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChatComponent } from './chat.component/chat/chat.component';
import { EventComponent } from './event.component/event/event.component';
import { SettingsComponent } from './settings/settings.component';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { LoadingComponent } from './loading/loading.component';
import { ControlService } from 'projects/below-deck/src/lib/services/controls.service';
import { MenuComponent } from './menu/menu.component';
import { LoadingService } from 'projects/below-deck/src/lib/services/rendering/loading.service';
import { GameComponent } from './game/game/game.component';

@NgModule({
  declarations: [
    AppComponent,
    ChatComponent,
    EventComponent,
    SettingsComponent,
    LoginComponent,
    MainComponent,
    LoadingComponent,
    MenuComponent,
    GameComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule],
  providers: [
    MyService,
    ChatService,
    EventMessageService,
    LoadingService,
    PlayerService,
    WebsocketServiceClient,
    ThreeService,
    ObservableService,
    ControlService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(
    private playerService: PlayerService,
    private controlService: ControlService,
    private loadingService: LoadingService,
    private threeService: ThreeService
  ) {}
}

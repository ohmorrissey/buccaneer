import { Component, OnInit } from '@angular/core';
import { WebsocketServiceClient, ChatService, Chat } from 'projects/below-deck/src/libs';

/** Chat component on the bottom right of the window.  */
@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit {

  /** List of chats mapped to chat service */
  public chats: Chat[] = [];

  constructor(
    private chatService: ChatService
  ) { }

  ngOnInit(): void {
    this.chats = this.chatService.chats;
    const chatInput = document.getElementById('chat-input') as HTMLInputElement;

    /** Listens for keyup event. Sends chat on enter. */
    chatInput.addEventListener('keyup', (event) => {
      console.log(event);
      if (event.key == 'Enter') {
        this.chatService.sendChat(chatInput.value);
        chatInput.value = '';
      }
    });
  }
}

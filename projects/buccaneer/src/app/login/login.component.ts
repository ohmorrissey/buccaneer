//src\app\app.component.ts
import { Component } from '@angular/core';
import { WebsocketServiceClient } from 'projects/below-deck/src/libs';
import { MyService } from 'projects/below-deck/src/lib/services/my.service';
import { ThreeService } from 'projects/below-deck/src/lib/services/rendering/THREE.service';
import { Router } from '@angular/router';
import {
  AppState,
  StateService,
} from 'projects/below-deck/src/lib/services/core/state.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  title: string = 'Buccaneer';
  content: string = '';
  userName: string = '';
  key: string = 'userName';

  constructor(
    private websocketServiceClient: WebsocketServiceClient,
    public myService: MyService,
    private threeService: ThreeService,
    private router: Router,
    private stateService: StateService
  ) {
    // this.threeService.init();
    // If three is already running, go back to main page
    // if (this.threeService.ThreeRunning) {
    //   this.router.navigate(['/']);
    // }

    // Populate username from storage
    this.userName = this.myService.getUserNameFromStorage();
  }

  /**
   * Callback for html button. Stores name from html input into myService.
   */
  login() {
    if (this.userName === '') {
      return;
    }
    this.myService.login(this.userName);
    this.router.navigate(['/menu']);
  }

  logout() {
    this.stateService.stateSubject.next(AppState.disconnected);
  }
}

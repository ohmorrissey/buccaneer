import { Component, OnInit } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';
import { Rudder } from 'projects/below-deck/src/lib/classes/boaty';
import { MyService } from 'projects/below-deck/src/lib/services/my.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent implements OnInit {
  public isMobile: boolean;
  constructor(
    public myService: MyService,
    public deviceService: DeviceDetectorService
  ) {
    this.isMobile = !this.deviceService.isDesktop();
  }

  ngOnInit(): void {}

  public fire(): void {
    console.warn('Fire not yet implemented');
  }

  public steerLeft(): void {
    this.myService.myBoaty?.steer(Rudder.left);
    console.log('left');
  }

  public steerRight(): void {
    this.myService.myBoaty?.steer(Rudder.right);
    console.log('right');
  }

  public steerStraight(): void {
    this.myService.myBoaty?.steer(Rudder.straight);
    console.log('straight');
  }

  public speedUp(): void {
    this.myService.myBoaty?.speedUp();
    console.log('speedUp');
  }

  public slowDown(): void {
    this.myService.myBoaty?.slowDown();
    console.log('slowDown');
  }
}

//src\app\app.component.ts
import { Component } from '@angular/core';
import { environment } from 'projects/below-deck/src/environments/environment';
import { ThreeService } from 'projects/below-deck/src/lib/services/rendering/THREE.service';
import { Router } from '@angular/router';
import packageJson from '../../package.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [
    './app.component.scss',
    './chat.component/chat/chat.component.scss',
  ],
})
export class AppComponent {
  public version = packageJson.version.toString();

  public showMenu = true;

  constructor(private router: Router, private threeService: ThreeService) {
    this.router.navigate(['/loading']);
    console.log('Author Nathan Morrissey. ', 'Environment: ', environment.NAME);

    document.addEventListener('keydown', (event) => {
      if (event.key !== 'Escape') {
        return;
      }

      if (location.pathname.startsWith('/menu')) {
        this.router.navigate(['/']);
      } else {
        this.router.navigate(['/menu']);
      }
    });
  }
}

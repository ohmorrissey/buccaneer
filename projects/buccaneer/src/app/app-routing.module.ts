import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { GameComponent } from './game/game/game.component';
import { LoadingComponent } from './loading/loading.component';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { MenuComponent } from './menu/menu.component';
import { SettingsComponent } from './settings/settings.component';

/** UNUSED. Originally for loading screen until html handling became dynamically loaded. */

const routes: Routes = [
  { path: '', component: GameComponent },
  {
    path: 'menu',
    component: MenuComponent,
    children: [
      { path: '', component: MainComponent },
      { path: 'login', component: LoginComponent },
      { path: 'settings', component: SettingsComponent },
    ],
  },
  { path: 'loading', component: LoadingComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { EventMessageService, Event } from 'projects/below-deck/src/lib/services/event.message.service';
import { WebsocketServiceClient } from 'projects/below-deck/src/libs';

/** Event notifications on bottom left of screen. */
@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss'],
})
export class EventComponent {

  /** Events mapped to event service. */
  public events: Event[] = [];

  constructor(
    private eventMessageService: EventMessageService
  ) {
    this.events = this.eventMessageService.events;
  }
}

BLUE='\033[0;34m'
NOCOLOUR='\033[0m'

echo -e echo "${BLUE}<=============> Building application <=============>${NOCOLOUR}"
ng build buccaneer
echo -e "${BLUE}<=============> Deploying application <=============>${NOCOLOUR}"
rm -rf /var/www/html/*
pwd
cp -r dist/buccaneer/* /var/www/html/
echo -e "${BLUE}<=============> Application Deployed <=============>${NOCOLOUR}"

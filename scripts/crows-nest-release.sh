if [ $# -eq 0 ]
  then
    echo "No Version Input Passed "
    npm version --help
    exit 1
fi

npm version -w projects/crows-nest $1 --no-git-tag

VERSION=`grep version projects/crows-nest/package.json | awk -F \" '{print $4}'`

TAG="crows-nest@"$VERSION

echo
echo "Releasing $TAG"
echo

git tag $TAG

git push origin $TAG

git add projects/crows-nest/package.json
git add projects/crows-nest/package-lock.json

git commit -m "Updating to $TAG"

git push

echo
echo "###########################"
echo "Tag and Package.json pushed"
echo "###########################"
echo